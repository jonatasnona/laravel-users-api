<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Password;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'App\Http\Controllers', 'middleware' => ['api']], function () {
    Route::post('signin', 'AuthController@login');
});

Route::group(['prefix' => 'users', 'namespace' => 'App\Http\Controllers', 'middleware' => ['api']], function () {
    Route::post('signup', 'UsersController@register');
    Route::get('{id}', 'UsersController@find');
    Route::put('{id}', 'UsersController@update');
    Route::delete('{id}', 'UsersController@remove');
});

Route::group(['prefix' => 'password', 'namespace' => 'App\Http\Controllers', 'middleware' => ['api']], function () {
    Route::post('forgot', 'PasswordController@forgotPassword');
    Route::post('reset', 'PasswordController@resetPassword');
});

Route::group(['prefix' => 'me', 'namespace' => 'App\Http\Controllers', 'middleware' => ['api']], function () {
    Route::get('', 'UsersController@getMe');
});
// Route::middleware('auth')->group(['prefix' => 'me', 'namespace' => 'App\Http\Controllers'], function () {
//     Route::get('me', 'UsersController@getMe');
// });



