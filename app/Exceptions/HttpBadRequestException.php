<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class HttpBadRequestException extends Exception
{
    protected $message = 'Something goes wrong.';
    protected $status = 400;
    protected $code = Response::HTTP_BAD_REQUEST;

    public function render(): Response
    {
        return response(["message" => $this->message], $this->status);
    }
}
