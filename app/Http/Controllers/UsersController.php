<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

use App\Models\User;
use App\Http\Requests\RegisterUser;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['register', 'find']]);
    }

    public function register(RegisterUser $request)
    {
        if (User::where('email', '=', $request->email)->exists()) {
            return response()->json(['message' => 'User already registered.'], 409);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        Log::info(UsersController::class, [$user]);
        return response()->json(['message' => 'User registered successfully.'], 201);
    }

    public function find($id)
    {
        try {
            $user = User::findOrFail($id);
            return response()->json($user);
        } catch (\Throwable $th) {
            return response()->json(['message'=>'User not found!'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);

            $user->name = $request->name;
            $user->save();

            return response()->json(['message' => 'User updated successfully.']);
        } catch (\Throwable $th) {
            return response()->json(['message'=>'User not found!'], 404);
        }
    }

    public function remove($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();

            return response()->json(['message' => 'User deleted successfully.']);
        } catch (\Throwable $th) {
            Log::info('hello3');
            return response()->json(['message'=>'User not found!'], 404);
        }
    }

    public function getMe(Request $request)
    {
        return response()->json(auth()->user());
    }
}
