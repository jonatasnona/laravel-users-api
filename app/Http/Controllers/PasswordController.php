<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Requests\ForgotPasswordUser;
use App\Http\Requests\ResetPasswordUser;
use App\Models\User;
use App\Models\PasswordReset;
use Mail;
use Hash;

class PasswordController extends Controller
{
    public function forgotPassword(ForgotPasswordUser $request)
    {
        if (!User::where('email', '=', $request->email)->exists()) {
            return response()->json(['message' => 'User not found.'], 404);
        }

        $token = Str::random(64);

        PasswordReset::create([
            'email' => $request->email,
            'token' => $token,
        ]);

        $email = $request->email;

        Mail::send('emails.forgotPassword', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });

        return response()->json(['message' => 'Reset password link was sent to your email.']);
    }

    public function resetPassword(ResetPasswordUser $request)
    {
        if (!PasswordReset::where('token', '=', $request->token)->exists()) {
            return response()->json(['message' => 'Token expired!'], 403);
        }

        $data = PasswordReset::where('token', '=', $request->token)->first();

        $user = User::where('email', '=', $data->email)->first();
        $user->password = Hash::make($request->password);
        $user->save();

        PasswordReset::where('email', '=', $data->email)->delete();

        return response()->json(['message' => 'Reset password successfully.']);
    }
}
