## Prerequisites

Before running the project, ensure you have the following prerequisites installed on your system:

- PHP 7.4 or higher
- Composer
- MySQL
- Docker (optional, if you prefer running the project with Docker)

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository

2. Install dependencies by navigating to the project directory and running the following command:
```bash
composer install
```

3. Create a copy of the `.env.example` file and name it `.env`. Update the necessary environment variables, such as database credentials and JWT secret. You will need to set up the MAIL_* envs.

4. Generate an application key by running the following command:
```bash
php artisan key:generate
```

5. Run database migrations to create the necessary tables:
```bash
php artisan migrate
```

6. (Optional) If you prefer running the project with Docker, use the provided `docker-compose.yml` file. Run the following command to start the containers (update your .env if needed):
```bash
docker-compose up
```

The application will be accessible at `http://localhost:8000`.

7. If you're not using Docker, start the Laravel development server:
```bash
php artisan serve
```

The application will be accessible at `http://localhost:8000`.

## API Endpoints

The following API endpoints are available:

- `POST /api/auth/signin`: Authenticate the user and generate a JWT token.
- `POST /api/password/forgot`: Send a password reset link to the user's email.
- `POST /api/users/register`: Register a new user.
- `GET /api/users/{id}`: Get a user by ID (must be authenticated).
- `GET /api/users/me`: Get a current user profile (must be authenticated).
- `PUT /api/users/{id}`: Update a user by ID (must be authenticated).
- `DELETE /api/users/{id}`: Delete a user by ID (must be authenticated).

Refer to the project's source code and documentation for more details on the API endpoints and their usage.

## Running Tests

To run the PHPUnit tests, use the following command:
```bash
php artisan test
```




